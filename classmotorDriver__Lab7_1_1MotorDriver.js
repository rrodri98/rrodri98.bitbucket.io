var classmotorDriver__Lab7_1_1MotorDriver =
[
    [ "__init__", "classmotorDriver__Lab7_1_1MotorDriver.html#a0a007d85480450fa5765584ae0c46e02", null ],
    [ "disable", "classmotorDriver__Lab7_1_1MotorDriver.html#a343a2805447f8c274a81fbc1420d23ee", null ],
    [ "enable", "classmotorDriver__Lab7_1_1MotorDriver.html#ae015319d831a0495ce031d43afd872c3", null ],
    [ "set_duty", "classmotorDriver__Lab7_1_1MotorDriver.html#a4fdfcb8789b743590119774a94a7aaba", null ],
    [ "CH1", "classmotorDriver__Lab7_1_1MotorDriver.html#ab63bf2ea8612a22c6bbba1eaa0ceba28", null ],
    [ "CH2", "classmotorDriver__Lab7_1_1MotorDriver.html#a615bbe8d2427dc915a8e4c799a417fc3", null ],
    [ "IN1_pin", "classmotorDriver__Lab7_1_1MotorDriver.html#a73ced796ba4d88992cb2f5c1ce78ce5e", null ],
    [ "IN2_pin", "classmotorDriver__Lab7_1_1MotorDriver.html#aa8317ca75cb5c650ad6d6c4a04b39e43", null ],
    [ "nSLEEP_pin", "classmotorDriver__Lab7_1_1MotorDriver.html#acd06029922cce5e128d26fa2bf070e9c", null ],
    [ "t_ch1", "classmotorDriver__Lab7_1_1MotorDriver.html#a2c76ebadca61f1be719dd97c1e2c71ed", null ],
    [ "t_ch2", "classmotorDriver__Lab7_1_1MotorDriver.html#a2b569a814fa2a1792c98da1952a57b8d", null ],
    [ "tim3", "classmotorDriver__Lab7_1_1MotorDriver.html#ac7bd20850ef8a560345ddd5831c2dcea", null ]
];