var UI__front__Lab6_8py =
[
    [ "sendChar", "UI__front__Lab6_8py.html#aaff525468a98ba9ea6b3f553be20e9be", null ],
    [ "fontsize", "UI__front__Lab6_8py.html#a61d4b023f67210f0816fbb54aa9ce986", null ],
    [ "label", "UI__front__Lab6_8py.html#a9497a35b359a6f814220fbc02cf0d586", null ],
    [ "list_of_omegafloats", "UI__front__Lab6_8py.html#ae046070396abe431ecce77c8ec71aff8", null ],
    [ "list_of_omegaREf_floats", "UI__front__Lab6_8py.html#ae00c8cbcd61da6e29ec50585e7e0dae9", null ],
    [ "list_of_timefloats", "UI__front__Lab6_8py.html#aeb5394a1da8d8d505bdde3cd45d3bcbe", null ],
    [ "omega", "UI__front__Lab6_8py.html#aa318c56a0894ed76c2df702a31a08167", null ],
    [ "omegaRef", "UI__front__Lab6_8py.html#ac03ab7db125544269e987b4a6be5fe01", null ],
    [ "ser", "UI__front__Lab6_8py.html#a897a6f3734057a3f847b1b2a4c1fb03e", null ],
    [ "time_", "UI__front__Lab6_8py.html#a49cd0244e80e8233aa80b1c168aecee7", null ],
    [ "val", "UI__front__Lab6_8py.html#a1413b2730c8260d17cb0c39e502a8b73", null ],
    [ "values", "UI__front__Lab6_8py.html#a69998ffd9129081b438084d76593812e", null ]
];