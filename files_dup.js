var files_dup =
[
    [ "BLE_Main.py", "BLE__Main_8py.html", "BLE__Main_8py" ],
    [ "BLE_Module.py", "BLE__Module_8py.html", [
      [ "BLE_Driver", "classBLE__Module_1_1BLE__Driver.html", "classBLE__Module_1_1BLE__Driver" ]
    ] ],
    [ "controller_Lab6.py", "controller__Lab6_8py.html", [
      [ "ClosedLoop", "classcontroller__Lab6_1_1ClosedLoop.html", "classcontroller__Lab6_1_1ClosedLoop" ],
      [ "ClosedLoopTask", "classcontroller__Lab6_1_1ClosedLoopTask.html", "classcontroller__Lab6_1_1ClosedLoopTask" ]
    ] ],
    [ "controller_Lab7.py", "controller__Lab7_8py.html", [
      [ "ClosedLoop", "classcontroller__Lab7_1_1ClosedLoop.html", "classcontroller__Lab7_1_1ClosedLoop" ],
      [ "ClosedLoopTask", "classcontroller__Lab7_1_1ClosedLoopTask.html", "classcontroller__Lab7_1_1ClosedLoopTask" ]
    ] ],
    [ "encoderDriver_Lab6.py", "encoderDriver__Lab6_8py.html", "encoderDriver__Lab6_8py" ],
    [ "encoderDriver_Lab7.py", "encoderDriver__Lab7_8py.html", "encoderDriver__Lab7_8py" ],
    [ "EncoderLabMain.py", "EncoderLabMain_8py.html", "EncoderLabMain_8py" ],
    [ "Fib_Sequence.py", "Fib__Sequence_8py.html", "Fib__Sequence_8py" ],
    [ "FSM_LED.py", "FSM__LED_8py.html", [
      [ "TaskBlinkLED", "classFSM__LED_1_1TaskBlinkLED.html", "classFSM__LED_1_1TaskBlinkLED" ]
    ] ],
    [ "FSM_virtualLED.py", "FSM__virtualLED_8py.html", [
      [ "TaskLEDtoggle", "classFSM__virtualLED_1_1TaskLEDtoggle.html", "classFSM__virtualLED_1_1TaskLEDtoggle" ]
    ] ],
    [ "IncrementalEncoder_03.py", "IncrementalEncoder__03_8py.html", [
      [ "UserTask", "classIncrementalEncoder__03_1_1UserTask.html", "classIncrementalEncoder__03_1_1UserTask" ]
    ] ],
    [ "main.py", "main_8py.html", "main_8py" ],
    [ "main_balance.py", "main__balance_8py.html", "main__balance_8py" ],
    [ "main_Lab6.py", "main__Lab6_8py.html", "main__Lab6_8py" ],
    [ "main_Lab7.py", "main__Lab7_8py.html", "main__Lab7_8py" ],
    [ "mainLab3.py", "mainLab3_8py.html", "mainLab3_8py" ],
    [ "MainLab4.py", "MainLab4_8py.html", "MainLab4_8py" ],
    [ "ME405_lab4.py", "ME405__lab4_8py.html", "ME405__lab4_8py" ],
    [ "ME405_lab4main.py", "ME405__lab4main_8py.html", "ME405__lab4main_8py" ],
    [ "motorDriver.py", "motorDriver_8py.html", "motorDriver_8py" ],
    [ "motorDriver_Lab6.py", "motorDriver__Lab6_8py.html", "motorDriver__Lab6_8py" ],
    [ "motorDriver_Lab7.py", "motorDriver__Lab7_8py.html", "motorDriver__Lab7_8py" ],
    [ "ReactionTimeLab_on_Nucleo.py", "ReactionTimeLab__on__Nucleo_8py.html", "ReactionTimeLab__on__Nucleo_8py" ],
    [ "ReactionTimeLab_Part_B.py", "ReactionTimeLab__Part__B_8py.html", "ReactionTimeLab__Part__B_8py" ],
    [ "ReturnChangeFunction_ME405_HW001.py", "ReturnChangeFunction__ME405__HW001_8py.html", "ReturnChangeFunction__ME405__HW001_8py" ],
    [ "shares.py", "shares_8py.html", "shares_8py" ],
    [ "shares_Lab6.py", "shares__Lab6_8py.html", "shares__Lab6_8py" ],
    [ "shares_Lab7.py", "shares__Lab7_8py.html", "shares__Lab7_8py" ],
    [ "TouchScreenDriver.py", "TouchScreenDriver_8py.html", "TouchScreenDriver_8py" ],
    [ "UI_EncoderLab.py", "UI__EncoderLab_8py.html", "UI__EncoderLab_8py" ],
    [ "UI_front.py", "UI__front_8py.html", "UI__front_8py" ],
    [ "UI_front_Lab6.py", "UI__front__Lab6_8py.html", "UI__front__Lab6_8py" ],
    [ "UI_front_Lab7.py", "UI__front__Lab7_8py.html", "UI__front__Lab7_8py" ],
    [ "Vendo.py", "Vendo_8py.html", "Vendo_8py" ],
    [ "Vendotron_FSM.py", "Vendotron__FSM_8py.html", "Vendotron__FSM_8py" ]
];