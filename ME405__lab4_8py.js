var ME405__lab4_8py =
[
    [ "TaskInternalTemp", "classME405__lab4_1_1TaskInternalTemp.html", "classME405__lab4_1_1TaskInternalTemp" ],
    [ "mcp9808", "classME405__lab4_1_1mcp9808.html", "classME405__lab4_1_1mcp9808" ],
    [ "__amb__", "ME405__lab4_8py.html#a3d8e3140f8129dbd526b8af8de2fede6", null ],
    [ "__i2c__", "ME405__lab4_8py.html#a34463d1dc896449b513e9a270aee4d7a", null ],
    [ "__n__", "ME405__lab4_8py.html#af8bb13ecece7e91552a25f0b68fe8e99", null ],
    [ "__nuc__", "ME405__lab4_8py.html#ac9f38bd299b9bc14f0a0e49b607b54d6", null ],
    [ "__sensor__", "ME405__lab4_8py.html#aeacc4e8a2a21e06b2004e252fddf11ba", null ],
    [ "amb_temps", "ME405__lab4_8py.html#ad67636f7cf5389b07914262a57200370", null ],
    [ "core_temps", "ME405__lab4_8py.html#ae8505ad4dfc09f4d2c6c9f6d28db0382", null ],
    [ "curr_time", "ME405__lab4_8py.html#a8a6287822bfbefb32d033f15b2de0c20", null ],
    [ "next_time", "ME405__lab4_8py.html#a945230ba15d796227cedef20f5178e40", null ],
    [ "start_time", "ME405__lab4_8py.html#ab173eddf57f8de3e0cd7f954ab403fd5", null ],
    [ "time", "ME405__lab4_8py.html#a6ac036c98755a2c89fb74c49326765d0", null ],
    [ "times", "ME405__lab4_8py.html#a86e00f7edcea2e6e75ae3bf83914bff2", null ]
];