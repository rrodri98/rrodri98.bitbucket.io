var ReactionTimeLab__Part__B_8py =
[
    [ "IC_callback", "ReactionTimeLab__Part__B_8py.html#a78ed3c213043b910246594242a852b5d", null ],
    [ "OC_callback", "ReactionTimeLab__Part__B_8py.html#a9c2e68cb09b23adafa21151e657f4c25", null ],
    [ "_avgReactionTot", "ReactionTimeLab__Part__B_8py.html#a8533791beb1013303e823c4dc0d82272", null ],
    [ "avg", "ReactionTimeLab__Part__B_8py.html#a7ccd0ddcdc08717adbe799b8789c9d0f", null ],
    [ "BP", "ReactionTimeLab__Part__B_8py.html#a18a6d4e337e36d547aca938c9c97b4b5", null ],
    [ "deltaT", "ReactionTimeLab__Part__B_8py.html#a9819298ba20b9e519eb65bc189044c8a", null ],
    [ "last_compare", "ReactionTimeLab__Part__B_8py.html#acc427a45aaa52277a97dd2a9280cbefd", null ],
    [ "pinA5", "ReactionTimeLab__Part__B_8py.html#aca59bfb4a81e561976345bbe2242a309", null ],
    [ "pinB3", "ReactionTimeLab__Part__B_8py.html#a1325f5b63fcc33121ec2a0921fa2873e", null ],
    [ "run", "ReactionTimeLab__Part__B_8py.html#a88a0bb37e8d800e9405b62dbe28e8f38", null ],
    [ "t2ch1", "ReactionTimeLab__Part__B_8py.html#ac3a9f226511d743602eab1447f20cb7a", null ],
    [ "t2ch2", "ReactionTimeLab__Part__B_8py.html#a172c189dd2254ab34a37ecf473ee78d9", null ],
    [ "tim2", "ReactionTimeLab__Part__B_8py.html#a3bca4fefea46ad8756c07d73e4c28b1a", null ],
    [ "timeBpressed", "ReactionTimeLab__Part__B_8py.html#a39d9d70a43593573ef61ee84debad932", null ]
];