var classencoderDriver__Lab7_1_1EncoderDriver =
[
    [ "__init__", "classencoderDriver__Lab7_1_1EncoderDriver.html#ad32f86986401c4bd93e7b9913354b8a0", null ],
    [ "get_delta", "classencoderDriver__Lab7_1_1EncoderDriver.html#a099479e2be7914caf2f57922d2ec2123", null ],
    [ "get_position", "classencoderDriver__Lab7_1_1EncoderDriver.html#a1a4ff001bbb20430e44df95c5b6c8cea", null ],
    [ "set_position", "classencoderDriver__Lab7_1_1EncoderDriver.html#a57316b22d65de83ded080865043ba783", null ],
    [ "update", "classencoderDriver__Lab7_1_1EncoderDriver.html#aa303d70c0fb25130665412dc8412f85d", null ],
    [ "CH1", "classencoderDriver__Lab7_1_1EncoderDriver.html#a977089362a5490b9492200f1d0a3af40", null ],
    [ "CH2", "classencoderDriver__Lab7_1_1EncoderDriver.html#a25f003f02e7c9f2074af0f50ecb221f0", null ],
    [ "counter", "classencoderDriver__Lab7_1_1EncoderDriver.html#a7005f620cd3d3aa0f63d3035b00df5df", null ],
    [ "delta", "classencoderDriver__Lab7_1_1EncoderDriver.html#aced4baf9adcf2d97d274fa9d7a34fdb2", null ],
    [ "delta_position", "classencoderDriver__Lab7_1_1EncoderDriver.html#a7a16658128af5ae5fbca03379e14ff9c", null ],
    [ "mag_delta", "classencoderDriver__Lab7_1_1EncoderDriver.html#ad5c3f7349e6e487b1695f9ad453095fd", null ],
    [ "period", "classencoderDriver__Lab7_1_1EncoderDriver.html#a695254eb53800a029bf98ef767df32c6", null ],
    [ "pin1", "classencoderDriver__Lab7_1_1EncoderDriver.html#aa8450683713553d94ab7d6797c41d54d", null ],
    [ "pin2", "classencoderDriver__Lab7_1_1EncoderDriver.html#adf19e6f5d41036e63926c482b9b73169", null ],
    [ "position", "classencoderDriver__Lab7_1_1EncoderDriver.html#aafd12141d3229e7fd34b3ff755716ce7", null ],
    [ "position_new", "classencoderDriver__Lab7_1_1EncoderDriver.html#a5c6ecf4a3b41c328910c717a24c377cc", null ],
    [ "position_old", "classencoderDriver__Lab7_1_1EncoderDriver.html#ac3a4cb39f771267488be490231f7232b", null ],
    [ "previous_counter", "classencoderDriver__Lab7_1_1EncoderDriver.html#ab908f7684378aacc3b6d225d38c31637", null ],
    [ "time", "classencoderDriver__Lab7_1_1EncoderDriver.html#a405cf6b1eee36ba81bf218ada4d11e2a", null ],
    [ "timer", "classencoderDriver__Lab7_1_1EncoderDriver.html#a8ee3bd1303673acbc11fd76755a6db74", null ]
];