var classmain__EncoderLab__I_1_1EncoderDriver =
[
    [ "__init__", "classmain__EncoderLab__I_1_1EncoderDriver.html#a541ee456540daff4c37228296cf6244c", null ],
    [ "get_delta", "classmain__EncoderLab__I_1_1EncoderDriver.html#a2fab5010e306c67d212bc27723125f80", null ],
    [ "get_position", "classmain__EncoderLab__I_1_1EncoderDriver.html#a31c62f3d5c0ba3f1ac704a4848a3707b", null ],
    [ "set_position", "classmain__EncoderLab__I_1_1EncoderDriver.html#a0a227b58dc7ff1f44b7b32ce91fca968", null ],
    [ "update", "classmain__EncoderLab__I_1_1EncoderDriver.html#a43a2dee7170f45e4250b39d39c374671", null ],
    [ "CH1", "classmain__EncoderLab__I_1_1EncoderDriver.html#a4e5d48d3d7d4ee016cea6b6a697d1c19", null ],
    [ "CH2", "classmain__EncoderLab__I_1_1EncoderDriver.html#a8bf35043d1649963cedc1c44143308e8", null ],
    [ "counter", "classmain__EncoderLab__I_1_1EncoderDriver.html#a798eecf1b8e02c062ae47dee0a07ee46", null ],
    [ "delta", "classmain__EncoderLab__I_1_1EncoderDriver.html#a93a5bb7254dd9fddf99f7693314913a3", null ],
    [ "delta_position", "classmain__EncoderLab__I_1_1EncoderDriver.html#a90bf150eb62be1a2bf7d3e20122c5763", null ],
    [ "mag_delta", "classmain__EncoderLab__I_1_1EncoderDriver.html#ac8487536c565796c07dc8148affef00e", null ],
    [ "period", "classmain__EncoderLab__I_1_1EncoderDriver.html#ace68ad0d837123c53c5bb7e714b578a8", null ],
    [ "pin1", "classmain__EncoderLab__I_1_1EncoderDriver.html#a05dddb164f7e8a63e005a891eb3896b3", null ],
    [ "pin2", "classmain__EncoderLab__I_1_1EncoderDriver.html#a57bc0ff53957dbd370c61fcf12d30d62", null ],
    [ "position", "classmain__EncoderLab__I_1_1EncoderDriver.html#a978924ed4a2a55f6bc4358b99b80f64b", null ],
    [ "previous_counter", "classmain__EncoderLab__I_1_1EncoderDriver.html#a485d6026b9e9ddcbbfb21c9aded6cb9a", null ],
    [ "time", "classmain__EncoderLab__I_1_1EncoderDriver.html#aa45f885e753fc6aeb2d042225d8e08e0", null ],
    [ "timer", "classmain__EncoderLab__I_1_1EncoderDriver.html#a2e1acd08d5da1c71864bb0936d2f3df4", null ]
];