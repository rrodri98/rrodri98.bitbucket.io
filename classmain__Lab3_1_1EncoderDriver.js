var classmain__Lab3_1_1EncoderDriver =
[
    [ "__init__", "classmain__Lab3_1_1EncoderDriver.html#aa27792c5517dbdd6cf94cdfb1c6f6044", null ],
    [ "get_delta", "classmain__Lab3_1_1EncoderDriver.html#a80b061e4b3da3f2ba27490b76035184c", null ],
    [ "get_position", "classmain__Lab3_1_1EncoderDriver.html#a3a6e2e40a31e2cc9a84933f007ce18f3", null ],
    [ "set_position", "classmain__Lab3_1_1EncoderDriver.html#a84f5cc88c8d75b20ef7b9fc273408d90", null ],
    [ "update", "classmain__Lab3_1_1EncoderDriver.html#ade8d20437bda224a64f34288b743ffdf", null ],
    [ "CH1", "classmain__Lab3_1_1EncoderDriver.html#a652adb0d17abad03cbe33f6b1529af94", null ],
    [ "CH2", "classmain__Lab3_1_1EncoderDriver.html#ae84aac8ea6d6c70076a1fad332339e1e", null ],
    [ "counter", "classmain__Lab3_1_1EncoderDriver.html#a80b8cc24db606c438a0d574b94bb9837", null ],
    [ "delta", "classmain__Lab3_1_1EncoderDriver.html#a0e55605e253f01f4981af17e9d0c3289", null ],
    [ "delta_position", "classmain__Lab3_1_1EncoderDriver.html#a9aaae5a3fa50141f5dc8c6e84d0b683f", null ],
    [ "mag_delta", "classmain__Lab3_1_1EncoderDriver.html#a1ae28f4c231879a31524c800d1c155ff", null ],
    [ "period", "classmain__Lab3_1_1EncoderDriver.html#ac54375299803b1955f952962d5fb495b", null ],
    [ "pin1", "classmain__Lab3_1_1EncoderDriver.html#ab7ff0bc7963ec20e63730984e912095c", null ],
    [ "pin2", "classmain__Lab3_1_1EncoderDriver.html#abd5fe027a908eba5b6ce72dbfcc21c15", null ],
    [ "position", "classmain__Lab3_1_1EncoderDriver.html#a9b7d4c95e1f20adf6f83c64a23ac4f21", null ],
    [ "previous_counter", "classmain__Lab3_1_1EncoderDriver.html#a1dd5337e142fc526958b42035167ca8c", null ],
    [ "time", "classmain__Lab3_1_1EncoderDriver.html#a0deccb0ebdba903a1c6acea94b1da66d", null ],
    [ "timer", "classmain__Lab3_1_1EncoderDriver.html#ac945549ff37715be385389a0e5a56a7a", null ]
];