var classmotorDriver__Lab6_1_1MotorDriver =
[
    [ "__init__", "classmotorDriver__Lab6_1_1MotorDriver.html#a849b352e10fef44d6b766a9f7e8ec612", null ],
    [ "disable", "classmotorDriver__Lab6_1_1MotorDriver.html#aedbf0d2a3c27e400d385aa87db5e1f49", null ],
    [ "enable", "classmotorDriver__Lab6_1_1MotorDriver.html#a7b5171e9943ab835377c188921246738", null ],
    [ "set_duty", "classmotorDriver__Lab6_1_1MotorDriver.html#aca222618d92078d9da110db379a66c17", null ],
    [ "CH1", "classmotorDriver__Lab6_1_1MotorDriver.html#a9011efe5e3335f24d5b5fb0426a61589", null ],
    [ "CH2", "classmotorDriver__Lab6_1_1MotorDriver.html#a2cca6aec9cac832c6ed75d5ff1818d4c", null ],
    [ "IN1_pin", "classmotorDriver__Lab6_1_1MotorDriver.html#abf74c1cd23261aa800884f23aa9eb8b3", null ],
    [ "IN2_pin", "classmotorDriver__Lab6_1_1MotorDriver.html#a0d3f80a5f18b271c8b430bdd522c58e0", null ],
    [ "nSLEEP_pin", "classmotorDriver__Lab6_1_1MotorDriver.html#ab3806d058d432e008d79ee290843de26", null ],
    [ "t_ch1", "classmotorDriver__Lab6_1_1MotorDriver.html#a6ec85ec23a6331ddd22728cd6b1d7b8e", null ],
    [ "t_ch2", "classmotorDriver__Lab6_1_1MotorDriver.html#a50033aa3ee03258172704e3935b788d4", null ],
    [ "tim3", "classmotorDriver__Lab6_1_1MotorDriver.html#acdef23702e2aa86b2caf4b60cda61cce", null ]
];