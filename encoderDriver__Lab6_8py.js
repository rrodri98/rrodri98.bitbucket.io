var encoderDriver__Lab6_8py =
[
    [ "EncoderDriver", "classencoderDriver__Lab6_1_1EncoderDriver.html", "classencoderDriver__Lab6_1_1EncoderDriver" ],
    [ "EncoderTask", "classencoderDriver__Lab6_1_1EncoderTask.html", "classencoderDriver__Lab6_1_1EncoderTask" ],
    [ "enc1", "encoderDriver__Lab6_8py.html#ae963fea4e88722d9766b7dd5a558ade7", null ],
    [ "enc2", "encoderDriver__Lab6_8py.html#a56856028e0ce87cb37a5a4db2e31d0c5", null ],
    [ "pin_IN1", "encoderDriver__Lab6_8py.html#a81b0b5ab587e8bced06886490ae69824", null ],
    [ "pin_IN2", "encoderDriver__Lab6_8py.html#aac9f76ef21d4d08f34b65d1830cf7c1d", null ],
    [ "pin_IN3", "encoderDriver__Lab6_8py.html#a8723806ee1f5f67da5eeafc3cccbcfb0", null ],
    [ "pin_IN4", "encoderDriver__Lab6_8py.html#aa04bca0594cda829c84126389ad7b373", null ],
    [ "task1", "encoderDriver__Lab6_8py.html#a7e3e8e6fe02ede17778f99393cf05101", null ],
    [ "task2", "encoderDriver__Lab6_8py.html#a1cf43d26443db51fda7557da8a12b778", null ],
    [ "taskList", "encoderDriver__Lab6_8py.html#a45d5e39912ff4568bef21e8d094b6caa", null ],
    [ "tim_1_2", "encoderDriver__Lab6_8py.html#ad43868150f8153af44dc4c08fc583c62", null ],
    [ "tim_3_4", "encoderDriver__Lab6_8py.html#a5d5b8b3fdf17fb8b7d4c3004deda0c38", null ]
];