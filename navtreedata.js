/*
 @licstart  The following is the entire license notice for the JavaScript code in this file.

 The MIT License (MIT)

 Copyright (C) 1997-2020 by Dimitri van Heesch

 Permission is hereby granted, free of charge, to any person obtaining a copy of this software
 and associated documentation files (the "Software"), to deal in the Software without restriction,
 including without limitation the rights to use, copy, modify, merge, publish, distribute,
 sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in all copies or
 substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
 BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

 @licend  The above is the entire license notice for the JavaScript code in this file
*/
var NAVTREE =
[
  [ "ME 305 + 405 Labs", "index.html", [
    [ "ME 305-Intro to Mechatronics", "page_ME305.html", [
      [ "Lab 0x01 Fibonacci Number Generator", "page_ME305.html#page_introLab1", [
        [ "Introduction", "page_ME305.html#sec_intro1", null ],
        [ "Fibonacci Function", "page_ME305.html#sec_fib", null ]
      ] ],
      [ "Lab 0x02 Blinking LED - Getting started with Hardware", "page_ME305.html#page_Lab2", [
        [ "Introduction", "page_ME305.html#sec_intro2", null ],
        [ "Task 1 - Virtual LED Pattern", "page_ME305.html#sec_led", null ],
        [ "Task 2 - Physical LED Pattern", "page_ME305.html#sec_led2", null ]
      ] ],
      [ "Lab 0x03 Incremental Encoders", "page_ME305.html#page_Lab3", [
        [ "Introduction", "page_ME305.html#sec_intro3", null ],
        [ "Task 1 - Encoder Driver", "page_ME305.html#sec_enc", null ],
        [ "Task 2 - Serial Communication", "page_ME305.html#sec_enc2", null ]
      ] ],
      [ "Lab 0x04 Extending Interface for Incremental Encoders", "page_ME305.html#page_Lab4", [
        [ "Introduction", "page_ME305.html#sec_intro4", null ],
        [ "Finite State Machine", "page_ME305.html#sec_fsm4", null ]
      ] ],
      [ "Lab 0x05 Bluetooth with Phone Interface", "page_ME305.html#page_Lab5", [
        [ "Introduction", "page_ME305.html#sec_intro5", null ],
        [ "Finite State Machine", "page_ME305.html#sec_fsm5", null ],
        [ "Task Diagram", "page_ME305.html#sec_td5", null ]
      ] ],
      [ "Lab 0x06 DC Motor Control", "page_ME305.html#page_Lab6", [
        [ "Introduction", "page_ME305.html#sec_intro6", null ],
        [ "Closed-Loop Speed Control", "page_ME305.html#sec_assign", null ]
      ] ],
      [ "Lab 0x07 Reference Tracking", "page_ME305.html#page_Lab7", [
        [ "Introduction", "page_ME305.html#sec_intro7", null ]
      ] ]
    ] ],
    [ "ME 405 - Mechatronics", "page_ME405.html", [
      [ "Homework 0x01-Python Review", "page_ME405.html#sec_HW", null ],
      [ "Lab 0x00 The Installation of Required Programs", "page_ME405.html#sec_LabZero", null ],
      [ "Lab 0x01 Vendotron", "page_ME405.html#sec_LabOne", null ],
      [ "Lab 0x02 Measure User Reaction Time", "page_ME405.html#sec_LabTwo", null ],
      [ "Lab 0x03 ADC", "page_ME405.html#sec_LabThree", null ],
      [ "Lab 0x04 Temperature Data Collection", "page_ME405.html#sec_LabFour", null ],
      [ "Lab 0x05 Balancing Ball on Platform Analysis", "page_ME405.html#sec_LabFive", null ],
      [ "Lab 0x06 Balancing Ball on Platform Simulation + Pole Placement", "page_ME405.html#sec_LabSix", null ],
      [ "Lab 0x0FF Term Project", "page_ME405.html#sec_LabSeven", [
        [ "Resistive Touch Screen Hardware", "page_ME405.html#sec_t1", null ],
        [ "Driving DC Motors with PWM", "page_ME405.html#sec_t2", null ],
        [ "Reading from Inertial Measurement Unit (IMU)", "page_ME405.html#sec_t3", null ],
        [ "Balancing the Ball", "page_ME405.html#sec_t4", null ]
      ] ],
      [ "Conclusion", "page_ME405.html#sec_eight", null ]
    ] ],
    [ "Classes", "annotated.html", [
      [ "Class List", "annotated.html", "annotated_dup" ],
      [ "Class Index", "classes.html", null ],
      [ "Class Hierarchy", "hierarchy.html", "hierarchy" ],
      [ "Class Members", "functions.html", [
        [ "All", "functions.html", "functions_dup" ],
        [ "Functions", "functions_func.html", null ],
        [ "Variables", "functions_vars.html", null ]
      ] ]
    ] ],
    [ "Files", "files.html", [
      [ "File List", "files.html", "files_dup" ]
    ] ]
  ] ]
];

var NAVTREEINDEX =
[
"BLE__Main_8py.html",
"classFSM__virtualLED_1_1TaskLEDtoggle.html",
"functions_m.html"
];

var SYNCONMSG = 'click to disable panel synchronisation';
var SYNCOFFMSG = 'click to enable panel synchronisation';