var searchData=
[
  ['mag_5fdelta_419',['mag_delta',['../classencoderDriver__Lab6_1_1EncoderDriver.html#a52716ea5710edd6e3de39d2d0b7726fd',1,'encoderDriver_Lab6.EncoderDriver.mag_delta()'],['../classencoderDriver__Lab7_1_1EncoderDriver.html#ad5c3f7349e6e487b1695f9ad453095fd',1,'encoderDriver_Lab7.EncoderDriver.mag_delta()']]],
  ['moe_5f1_420',['moe_1',['../main__balance_8py.html#ab917993a918fb9b0d5db3aec6482996c',1,'main_balance.moe_1()'],['../main__Lab6_8py.html#a7f949ce7d9871813f0395049a903453b',1,'main_Lab6.moe_1()'],['../main__Lab7_8py.html#a61072c8f08d6aaba127bca6a1a93985f',1,'main_Lab7.moe_1()']]],
  ['moe_5f2_421',['moe_2',['../main__balance_8py.html#a100011d8a462986e4f07fded77943597',1,'main_balance']]],
  ['motor_422',['Motor',['../classcontroller__Lab6_1_1ClosedLoopTask.html#a784e44d4040c80b80d10a326c9c2f305',1,'controller_Lab6.ClosedLoopTask.Motor()'],['../classcontroller__Lab7_1_1ClosedLoopTask.html#ae5bf2be30b1ca082a5f987f044241892',1,'controller_Lab7.ClosedLoopTask.Motor()']]],
  ['myuart_423',['myuart',['../classIncrementalEncoder__03_1_1UserTask.html#a1902126d4d9653960518b59bbdf4ed23',1,'IncrementalEncoder_03.UserTask.myuart()'],['../main__Lab6_8py.html#a67b77350254805edb6a2322c25d53877',1,'main_Lab6.myuart()'],['../main__Lab7_8py.html#a9c2d606ed418c2e4c51f5f9b841a3ad1',1,'main_Lab7.myuart()'],['../mainLab3_8py.html#ab0702202eacf9c80d81b72eccb47bc5c',1,'mainLab3.myuart()'],['../MainLab4_8py.html#a6d7de4f2eef6165b0979325dd6587bb5',1,'MainLab4.myuart()']]]
];
