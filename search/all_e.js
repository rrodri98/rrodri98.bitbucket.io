var searchData=
[
  ['oc_5fcallback_126',['OC_callback',['../ReactionTimeLab__Part__B_8py.html#a9c2e68cb09b23adafa21151e657f4c25',1,'ReactionTimeLab_Part_B']]],
  ['omega_127',['omega',['../UI__front__Lab6_8py.html#aa318c56a0894ed76c2df702a31a08167',1,'UI_front_Lab6.omega()'],['../UI__front__Lab7_8py.html#a06c816bc436d4dac792564357f927a95',1,'UI_front_Lab7.omega()']]],
  ['omega_5fmeas_128',['omega_meas',['../main__Lab6_8py.html#aa2bec3a5ba06cb84fae41e66e4ab6da9',1,'main_Lab6.omega_meas()'],['../main__Lab7_8py.html#aaddd7ccd0598f7f6a338b35f90b0deab',1,'main_Lab7.omega_meas()'],['../shares__Lab6_8py.html#a0ee46a304ca503dadd2f1e85648819f3',1,'shares_Lab6.omega_meas()'],['../shares__Lab7_8py.html#a07674b88008b93185176751a50a3972d',1,'shares_Lab7.omega_meas()']]],
  ['omega_5fref_129',['omega_ref',['../main__Lab6_8py.html#a1f95a0c9962de23e1805d6287808859e',1,'main_Lab6.omega_ref()'],['../main__Lab7_8py.html#a69f9832d8cabf9b2b92c56e61144b5a9',1,'main_Lab7.omega_ref()'],['../shares__Lab6_8py.html#a4fc9cb0ca890f5720c57eda6e875ff88',1,'shares_Lab6.omega_ref()'],['../shares__Lab7_8py.html#a64042948d1cb814552b8d9ec40a64686',1,'shares_Lab7.omega_ref()']]],
  ['omegaref_130',['omegaRef',['../UI__front__Lab6_8py.html#ac03ab7db125544269e987b4a6be5fe01',1,'UI_front_Lab6.omegaRef()'],['../UI__front__Lab7_8py.html#a9fc987f52806721f13b66d89befe57d2',1,'UI_front_Lab7.omegaRef()']]],
  ['overflow_131',['Overflow',['../ReactionTimeLab__on__Nucleo_8py.html#ad8cb708afcfd466dde4f6d20ba13fdfd',1,'ReactionTimeLab_on_Nucleo']]]
];
