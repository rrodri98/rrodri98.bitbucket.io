var annotated_dup =
[
    [ "BLE_Module", null, [
      [ "BLE_Driver", "classBLE__Module_1_1BLE__Driver.html", "classBLE__Module_1_1BLE__Driver" ]
    ] ],
    [ "controller_Lab6", null, [
      [ "ClosedLoop", "classcontroller__Lab6_1_1ClosedLoop.html", "classcontroller__Lab6_1_1ClosedLoop" ],
      [ "ClosedLoopTask", "classcontroller__Lab6_1_1ClosedLoopTask.html", "classcontroller__Lab6_1_1ClosedLoopTask" ]
    ] ],
    [ "controller_Lab7", null, [
      [ "ClosedLoop", "classcontroller__Lab7_1_1ClosedLoop.html", "classcontroller__Lab7_1_1ClosedLoop" ],
      [ "ClosedLoopTask", "classcontroller__Lab7_1_1ClosedLoopTask.html", "classcontroller__Lab7_1_1ClosedLoopTask" ]
    ] ],
    [ "encoderDriver_Lab6", null, [
      [ "EncoderDriver", "classencoderDriver__Lab6_1_1EncoderDriver.html", "classencoderDriver__Lab6_1_1EncoderDriver" ],
      [ "EncoderTask", "classencoderDriver__Lab6_1_1EncoderTask.html", "classencoderDriver__Lab6_1_1EncoderTask" ]
    ] ],
    [ "encoderDriver_Lab7", null, [
      [ "EncoderDriver", "classencoderDriver__Lab7_1_1EncoderDriver.html", "classencoderDriver__Lab7_1_1EncoderDriver" ],
      [ "EncoderTask", "classencoderDriver__Lab7_1_1EncoderTask.html", "classencoderDriver__Lab7_1_1EncoderTask" ]
    ] ],
    [ "EncoderLabMain", null, [
      [ "EncoderDriver", "classEncoderLabMain_1_1EncoderDriver.html", "classEncoderLabMain_1_1EncoderDriver" ],
      [ "EncoderTask", "classEncoderLabMain_1_1EncoderTask.html", "classEncoderLabMain_1_1EncoderTask" ]
    ] ],
    [ "FSM_LED", null, [
      [ "TaskBlinkLED", "classFSM__LED_1_1TaskBlinkLED.html", "classFSM__LED_1_1TaskBlinkLED" ]
    ] ],
    [ "FSM_virtualLED", null, [
      [ "TaskLEDtoggle", "classFSM__virtualLED_1_1TaskLEDtoggle.html", "classFSM__virtualLED_1_1TaskLEDtoggle" ]
    ] ],
    [ "IncrementalEncoder_03", null, [
      [ "UserTask", "classIncrementalEncoder__03_1_1UserTask.html", "classIncrementalEncoder__03_1_1UserTask" ]
    ] ],
    [ "ME405_lab4", null, [
      [ "mcp9808", "classME405__lab4_1_1mcp9808.html", "classME405__lab4_1_1mcp9808" ],
      [ "TaskInternalTemp", "classME405__lab4_1_1TaskInternalTemp.html", "classME405__lab4_1_1TaskInternalTemp" ]
    ] ],
    [ "motorDriver", null, [
      [ "MotorDriver", "classmotorDriver_1_1MotorDriver.html", "classmotorDriver_1_1MotorDriver" ]
    ] ],
    [ "motorDriver_Lab6", null, [
      [ "MotorDriver", "classmotorDriver__Lab6_1_1MotorDriver.html", "classmotorDriver__Lab6_1_1MotorDriver" ]
    ] ],
    [ "motorDriver_Lab7", null, [
      [ "MotorDriver", "classmotorDriver__Lab7_1_1MotorDriver.html", "classmotorDriver__Lab7_1_1MotorDriver" ]
    ] ],
    [ "TouchScreenDriver", null, [
      [ "ScreenDriver", "classTouchScreenDriver_1_1ScreenDriver.html", "classTouchScreenDriver_1_1ScreenDriver" ]
    ] ]
];