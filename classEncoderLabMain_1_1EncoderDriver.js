var classEncoderLabMain_1_1EncoderDriver =
[
    [ "__init__", "classEncoderLabMain_1_1EncoderDriver.html#ae95dafdd63d05cc0eef2bd90c6d5866c", null ],
    [ "get_delta", "classEncoderLabMain_1_1EncoderDriver.html#a011aebeced18786d69a366a2c5f4fa75", null ],
    [ "get_position", "classEncoderLabMain_1_1EncoderDriver.html#a8606dee011c7e6bcbcb0263b387cb0e1", null ],
    [ "set_position", "classEncoderLabMain_1_1EncoderDriver.html#a7ac925339e256f732f82c0153b06d5c2", null ],
    [ "update", "classEncoderLabMain_1_1EncoderDriver.html#a76e286eb08896db6b53e5faf5ca9ec3c", null ],
    [ "CH1", "classEncoderLabMain_1_1EncoderDriver.html#a639e934f72e4d500c2bfb096e502cb13", null ],
    [ "CH2", "classEncoderLabMain_1_1EncoderDriver.html#a9ba3e5a6ae577adcfc7798e4a8fa382b", null ],
    [ "counter", "classEncoderLabMain_1_1EncoderDriver.html#a18b8c4e2af34bf8b36c4d74d45593b81", null ],
    [ "delta", "classEncoderLabMain_1_1EncoderDriver.html#a70042c71ed476c5358513f365eec15aa", null ],
    [ "delta_position", "classEncoderLabMain_1_1EncoderDriver.html#a659ec92c46b66cdcae57e69e8b2462fd", null ],
    [ "mag_delta", "classEncoderLabMain_1_1EncoderDriver.html#a356c6cda644e5a32fc0cc9ad028aa4ba", null ],
    [ "period", "classEncoderLabMain_1_1EncoderDriver.html#a87fd20d638ef569d306c471e8fab183e", null ],
    [ "pin1", "classEncoderLabMain_1_1EncoderDriver.html#ae91632e6722cf0edba1857a7a30ab011", null ],
    [ "pin2", "classEncoderLabMain_1_1EncoderDriver.html#a5bede7a9a45c55d2def67e05d03f7f60", null ],
    [ "position", "classEncoderLabMain_1_1EncoderDriver.html#a340293a2201405e2da348805ef50a1b2", null ],
    [ "previous_counter", "classEncoderLabMain_1_1EncoderDriver.html#acc15904d62186cad91c135ef63525ed8", null ],
    [ "time", "classEncoderLabMain_1_1EncoderDriver.html#ab24c9b36937e2db2608361678d3ed31b", null ],
    [ "timer", "classEncoderLabMain_1_1EncoderDriver.html#a0c9f54a43fab4c773581e7999fb2ce83", null ]
];