var ME405__lab4main_8py =
[
    [ "__amb__", "ME405__lab4main_8py.html#a39ba1c3aafae68c13653486928de8ddd", null ],
    [ "__i2c__", "ME405__lab4main_8py.html#a228848454376379f377c2cb01abc26cd", null ],
    [ "__n__", "ME405__lab4main_8py.html#ab21c69e80a93e812727fc5cadda9bdfd", null ],
    [ "__nuc__", "ME405__lab4main_8py.html#a926f0b0661fdb4b88a0ff23de3e7e9b5", null ],
    [ "__sensor__", "ME405__lab4main_8py.html#ad49b7d917c4fa6c102b83b9be280ee06", null ],
    [ "amb_temps", "ME405__lab4main_8py.html#a676960771f192f703cec584b7b0cf2dc", null ],
    [ "core_temps", "ME405__lab4main_8py.html#ad7bcbfdcb792ef68695f62ae38863e47", null ],
    [ "curr_time", "ME405__lab4main_8py.html#a73079385f3ecbeca29c68920afe52e53", null ],
    [ "next_time", "ME405__lab4main_8py.html#a0d5aeedc0efccf4060c20244e2f9e103", null ],
    [ "start_time", "ME405__lab4main_8py.html#ae9a2f957522bd854ca712716eb1e9cbf", null ],
    [ "time", "ME405__lab4main_8py.html#a8570bd1366f7176beb675b7598498248", null ],
    [ "times", "ME405__lab4main_8py.html#a6c51caa5890286b035ed91a7cc2059a7", null ]
];