var encoderDriver__Lab7_8py =
[
    [ "EncoderDriver", "classencoderDriver__Lab7_1_1EncoderDriver.html", "classencoderDriver__Lab7_1_1EncoderDriver" ],
    [ "EncoderTask", "classencoderDriver__Lab7_1_1EncoderTask.html", "classencoderDriver__Lab7_1_1EncoderTask" ],
    [ "enc1", "encoderDriver__Lab7_8py.html#a1fcba46f1a20ca9bcd7da7c252c9cec7", null ],
    [ "enc2", "encoderDriver__Lab7_8py.html#af9bf2ee27bf7a77f90b94dc1b3518ec0", null ],
    [ "pin_IN1", "encoderDriver__Lab7_8py.html#a170a765c429ac14dc39c8651e4982e08", null ],
    [ "pin_IN2", "encoderDriver__Lab7_8py.html#ae401e4ca927e39010f2c14272e9f656b", null ],
    [ "pin_IN3", "encoderDriver__Lab7_8py.html#a4b88cce13d96ef0d96c12faa28f980c8", null ],
    [ "pin_IN4", "encoderDriver__Lab7_8py.html#a0aad82107501e110611d81588b190929", null ],
    [ "task1", "encoderDriver__Lab7_8py.html#a8d2b485da93bf309f6e47f03be5bafd1", null ],
    [ "task2", "encoderDriver__Lab7_8py.html#a1b481c4d316821311895fd4b07c243d4", null ],
    [ "taskList", "encoderDriver__Lab7_8py.html#ac0ee69a8b5672f40c808bd8f4cb72fc3", null ],
    [ "tim_1_2", "encoderDriver__Lab7_8py.html#a436c7de1a1c194f6b8d669bc01cd8555", null ],
    [ "tim_3_4", "encoderDriver__Lab7_8py.html#a595475cf3b16f4fef233756783180fd7", null ]
];