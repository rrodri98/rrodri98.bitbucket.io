var classencoderDriver__Lab6_1_1EncoderDriver =
[
    [ "__init__", "classencoderDriver__Lab6_1_1EncoderDriver.html#a8aa4abeeaece873319e420c4af5f03ee", null ],
    [ "get_delta", "classencoderDriver__Lab6_1_1EncoderDriver.html#a633fd1e356805906643205f88688dbcf", null ],
    [ "get_position", "classencoderDriver__Lab6_1_1EncoderDriver.html#af5c6d8b13e251d74d8e99d24c9cdb82e", null ],
    [ "set_position", "classencoderDriver__Lab6_1_1EncoderDriver.html#a1ab62f29993df4be45a3eedf5e7303ce", null ],
    [ "update", "classencoderDriver__Lab6_1_1EncoderDriver.html#a0891d57a7fd587d93e885cf48f410f12", null ],
    [ "CH1", "classencoderDriver__Lab6_1_1EncoderDriver.html#a1e98fd21296c5fd0a175a92fb4c00777", null ],
    [ "CH2", "classencoderDriver__Lab6_1_1EncoderDriver.html#a54899985d75aeddace962b11d488a63e", null ],
    [ "counter", "classencoderDriver__Lab6_1_1EncoderDriver.html#abe5bc16abcc18c8b2dab91d50babb6d8", null ],
    [ "delta", "classencoderDriver__Lab6_1_1EncoderDriver.html#acafe5351ef2b77a27a571be2e56c8a59", null ],
    [ "delta_position", "classencoderDriver__Lab6_1_1EncoderDriver.html#a0b30b566a98480d69290034a247ee7a5", null ],
    [ "mag_delta", "classencoderDriver__Lab6_1_1EncoderDriver.html#a52716ea5710edd6e3de39d2d0b7726fd", null ],
    [ "period", "classencoderDriver__Lab6_1_1EncoderDriver.html#a7706afd119480b1331953b29d9ebef61", null ],
    [ "pin1", "classencoderDriver__Lab6_1_1EncoderDriver.html#a7290b68c95a2be458c6c1724ede1a407", null ],
    [ "pin2", "classencoderDriver__Lab6_1_1EncoderDriver.html#ac31143ca59d8a536913bec64a6f0fe20", null ],
    [ "position", "classencoderDriver__Lab6_1_1EncoderDriver.html#a003297b852926a47abe8d7c8a4eab58d", null ],
    [ "position_new", "classencoderDriver__Lab6_1_1EncoderDriver.html#a5c8beaee7234f79eff3ca516cd4b441c", null ],
    [ "position_old", "classencoderDriver__Lab6_1_1EncoderDriver.html#a79476ba8dbec292431d791d44e81f375", null ],
    [ "previous_counter", "classencoderDriver__Lab6_1_1EncoderDriver.html#ad5da965ca61a5da3d22e32f06c1c4290", null ],
    [ "time", "classencoderDriver__Lab6_1_1EncoderDriver.html#ad4ee6bd2aaf66b0a3ab318e03d430861", null ],
    [ "timer", "classencoderDriver__Lab6_1_1EncoderDriver.html#a642fe342ac7c0901f9ac01f48fc20e7f", null ]
];